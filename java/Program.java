package com.bignerdranch.android.whattowatch;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KORISNIK on 5/2/2018.
 */

public class Program extends AppCompatActivity{
    String Ime;
    String LinkStranice;
    String LinkDijela;
   public  List<Emisija> Emisije = new ArrayList<>();
    public Program(){
        Ime = "";
        LinkStranice = "";
    }

    private List<String> filtriranje (String naziv, String opis, String informacija){
        String zanr="";
        String pom="";
        String glumci="";
        List<String> rezultat = new ArrayList<>();


        if(informacija.length() != 0 || naziv.length() !=0 || opis.length() != 0 ) {
            for (int i = naziv.length() + opis.length() + 1; i < informacija.length(); i++) {
                if ((informacija.charAt(i) >= (int) 'A' && informacija.charAt(i) <= (int) 'Z') || informacija.charAt(i) == ' ' || informacija.charAt(i) == ',') {
                    pom += informacija.charAt(i);
                }
                if (informacija.charAt(i) > (int) 'a' && informacija.charAt(i) < (int) 'z') break;
            }
            for (int i = 0; i < pom.length() - 1; i++) zanr += pom.charAt(i);

            for (int i = naziv.length() + opis.length() + zanr.length() + 1; i < informacija.length(); i++) {

                if(zanr.length() != 0) {
                    if ((informacija.charAt(i) == zanr.charAt(0) && informacija.charAt(i + 1) == zanr.charAt(1)))
                        break;
                }
                glumci += informacija.charAt(i);
            }

            //List<String> rezultat = new ArrayList<>();
            rezultat.add(zanr);
            rezultat.add(glumci);
        }
        else{
            rezultat.add("");
            rezultat.add("");
        }

        return rezultat;
    }


    public List<Emisija> PreuzimanjeInfo(String ime) {

        final String Ime = ime;
        //if(Ime == "TV1000"){
           if( Ime.equals("TV1000")){
            LinkStranice="https://mojtv.hr/kanal/tv-program/43/tv1000.aspx";
            LinkDijela =  "div#chbig43.overflow a.show";

        }
        else if( Ime.equals("FOX")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/319/fox.aspx";
               LinkDijela =  "div#chbig319.overflow a.show";
           }
           else if( Ime.equals("FOXlife")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/69/fox-life.aspx";
               LinkDijela =  "div#chbig69.overflow a.show";
           }
           else if( Ime.equals("CineStar")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/106/cinestar-tv.aspx";
               LinkDijela =  "div#chbig106.overflow a.show";
           }
           else if( Ime.equals("TV1")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/217/tv1.aspx";
               LinkDijela =  "div#chbig217.overflow a.show";
           }
           else if( Ime.equals("RTL")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/4/rtl.aspx";
               LinkDijela =  "div#chbig4.overflow a.show";
           }
           else if( Ime.equals("FoxMovie")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/286/fox-movies.aspx";
               LinkDijela =  "div#chbig286.overflow a.show";
           }
           else if( Ime.equals("NovaTV")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/3/nova-tv.aspx";
               LinkDijela =  "div#chbig3.overflow a.show";
           }
           else if( Ime.equals("Hayat")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/35/hayat-tv.aspx";
               LinkDijela =  "div#chbig35.overflow a.show";
           }
           else if( Ime.equals("FTV")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/25/ftv.aspx";
               LinkDijela =  "div#chbig25.overflow a.show";
           }
           else if( Ime.equals("FaceTV")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/319/fox.aspx";
               LinkDijela =  "div#chbig319.overflow a.show";
           }
           else if( Ime.equals("OBN")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/25/ftv.aspx";
               LinkDijela =  "div#chbig25.overflow a.show";
           }
           else if( Ime.equals("HRT1")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/1/htv1.aspx";
               LinkDijela =  "div#chbig1.overflow a.show";
           }
           else if( Ime.equals("Pink")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/27/pink.aspx";
               LinkDijela =  "div#chbig27.overflow a.show";
           }
           else if( Ime.equals("AXN")){
               LinkStranice="https://mojtv.hr/kanal/tv-program/319/axn.aspx";
               LinkDijela =  "div#chbig112.overflow a.show";
           }

         List<Emisija> NovaEmisija1 = new ArrayList<>();
        Thread thread = new Thread(new Runnable() {
            //List<Emisija> NovaEmisija = new ArrayList<>();
            @Override

            public void run () {
                List<Emisija> NovaEmisija = new ArrayList<>();
                try {
                    //List<Emisija> NovaEmisija = new ArrayList<>();

                    String text = "https://mojtv.hr/kanal/tv-program/319/fox.aspx";

                    Document konekcija = Jsoup.connect(LinkStranice).get();
                    Log.v("Konektovano", "str");


                    for(org.jsoup.nodes.Element emisija : konekcija.select(LinkDijela)) {

                        String naziv = emisija.select("strong.title").text();
                        String opis = emisija.select("strong.desc").text();
                        String informacija = emisija.select("span").text();
                        String vrijeme = emisija.select("em.time").text();
                        String zanr = filtriranje(naziv, opis, informacija).get(0);
                        String glumci = filtriranje(naziv, opis, informacija).get(1);

                        Log.v("naziv", naziv);
                        Log.v("opis", opis);
                        Log.v("zanr", zanr);
                        Log.v("vrijeme", vrijeme);
                        Log.v("glumci", glumci);
                        Log.v("-", "------------------------------");


                        NovaEmisija.add(new Emisija(naziv, opis, vrijeme, Ime, zanr, glumci));

                        Log.v("vel", Integer.toString(NovaEmisija.size()));

                    }
                    Emisije = NovaEmisija;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    Log.v("Catch:", "Palo");
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("Pokrenuto", "hahe");
                    }

                });


                Log.v("Velicina123", Integer.toString(Emisije.size()));

            }

        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.v("Velicina1", Integer.toString(Emisije.size()));
        return Emisije;
    }
}
