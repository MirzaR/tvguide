package com.bignerdranch.android.whattowatch;

import java.util.UUID;

/**
 * Created by KORISNIK on 5/15/2018.
 */

public class NazivPrograma {
    String mIme;
    private UUID mId;
    public NazivPrograma(String ime) {
        mIme = ime;
        mId = UUID.randomUUID();
    }

    public String getIme() {
        return mIme;
    }

    public void setIme(String ime) {
        mIme = ime;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }
}
