package com.bignerdranch.android.whattowatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ProgramActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);

    }

    @Override
    protected Fragment createFragment() {

        return new ProgramiFragment();

    }


}
