package com.bignerdranch.android.whattowatch;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by KORISNIK on 5/17/2018.
 */

public class SpisakPrograma {
    private static SpisakPrograma sSpisakPrograma;
    private List<NazivPrograma> mSviProgrami;

    public static SpisakPrograma get(Context context) {
        if (sSpisakPrograma == null) {
            sSpisakPrograma = new SpisakPrograma(context);
        }
        return sSpisakPrograma;
    }
    private SpisakPrograma(Context context) {
        mSviProgrami = new ArrayList<>();
        mSviProgrami.add(new NazivPrograma("TV1000"));
        mSviProgrami.add(new NazivPrograma("FOX"));
        mSviProgrami.add(new NazivPrograma("FOXlife"));
        mSviProgrami.add(new NazivPrograma("CineStar"));
        mSviProgrami.add(new NazivPrograma("TV1"));
        mSviProgrami.add(new NazivPrograma("RTL"));
        mSviProgrami.add(new NazivPrograma("FoxMovie"));
        mSviProgrami.add(new NazivPrograma("NovaTV"));
        mSviProgrami.add(new NazivPrograma("Hayat"));
        mSviProgrami.add(new NazivPrograma("FTV"));
        mSviProgrami.add(new NazivPrograma("FaceTV"));
        mSviProgrami.add(new NazivPrograma("OBN"));
        mSviProgrami.add(new NazivPrograma("HRT1"));
        mSviProgrami.add(new NazivPrograma("Pink"));
        mSviProgrami.add(new NazivPrograma("AXN"));
    }

    public List<NazivPrograma> getSviProgrami() {
        return mSviProgrami;
    }
    public NazivPrograma getCrime(UUID id) {
        for (NazivPrograma nazivPrograma : mSviProgrami) {
            if (nazivPrograma.getId().equals(id)) {
                return nazivPrograma;
            }
        }
        return null;
    }
}
