package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;


/**
 * Created by User on 05.06.2018..
 */

public class EmisijaInfoPager extends AppCompatActivity
implements EmisijaInfoFragment.Callbacks{

    private ViewPager mViewPager;
    private List<Emisija> mEmisije;

    public static final String EXTRA_PROGRAM_IME =
            "com.example.korisnik.informacije.ime_programa";
    public static final String EXTRA_EMISIJA_IME =
            "com.example.korisnik.informacije.ime_emisije";

    public static Intent newIntent(Context packageContext, String imeEmisije, String imePrograma) {
        Intent intent = new Intent(packageContext, EmisijaInfoPager.class);
        intent.putExtra(EXTRA_EMISIJA_IME, imeEmisije);
        intent.putExtra(EXTRA_PROGRAM_IME, imePrograma);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recenzija_pager);

        final String emisijaIme = (String) getIntent()
                .getSerializableExtra(EXTRA_EMISIJA_IME);
        final String programIme = (String) getIntent()
                .getSerializableExtra(EXTRA_PROGRAM_IME);

        mViewPager = (ViewPager) findViewById(R.id.recenzija_view_pager);
        mEmisije = new Program().PreuzimanjeInfo(programIme);
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Emisija emisija = mEmisije.get(position);
                return EmisijaInfoFragment.newInstance(emisija.getNaziv(), emisija.getProgram());
            }
            @Override
            public int getCount() {
                return mEmisije.size();
            }
        });

        for (int i = 0; i < mEmisije.size(); i++) {
            if (mEmisije.get(i).getNaziv().equals(emisijaIme)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }

    }

}

