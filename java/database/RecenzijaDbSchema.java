package com.bignerdranch.android.whattowatch.database;

/**
 * Created by User on 05.06.2018..
 */

public class RecenzijaDbSchema {
    public static final class RecenzijaTable {
        public static final String NAME = "recenzija";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String ImeEmisije = "naziv";
            public static final String Datum = "datum";
            public static final String Vrijeme = "vrijeme";
            public static final String Zanr = "zanr";
            public static final String Glumci = "glumci";
            public static final String Opis = "opis";
            public static final String Komentar = "komentar";
            public static final String Ocjena = "ocjena";
        }
    }
}
