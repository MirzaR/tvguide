package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

/**
 * Created by KORISNIK on 6/5/2018.
 */

public class RecenzijaPagerActivity extends AppCompatActivity
implements RecenzijaInfoFragment.Callbacks{
    private ViewPager mViewPager;
    private List<Recenzija> mRecenzije;

    private static final String EXTRA_RECENZIJA_ID =
            "com.example.korisnik.informacije.recenzija_id";

    public static Intent newIntent(Context packageContext, UUID recenzijaId) {
        Intent intent = new Intent(packageContext, RecenzijaPagerActivity.class);
        intent.putExtra(EXTRA_RECENZIJA_ID, recenzijaId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recenzija_pager);

        UUID recenzijaId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_RECENZIJA_ID);

        mViewPager = (ViewPager) findViewById(R.id.recenzija_view_pager);
        BazaRecenzija bazaRecenzija = BazaRecenzija.get(this);
        mRecenzije = bazaRecenzija.getRecenzije();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Recenzija recenzija = mRecenzije.get(position);
                return RecenzijaInfoFragment.newInstance(recenzija.getId());
            }
            @Override
            public int getCount() {
                return mRecenzije.size();
            }
        });

        for (int i = 0; i < mRecenzije.size(); i++) {
            if (mRecenzije.get(i).getId().equals(recenzijaId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

    @Override
    public void onRecenzijaUpdated (Recenzija recenzija){

    }
}
