package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.icu.util.GregorianCalendar;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 05.06.2018..
 */

public class EmisijaInfoFragment extends Fragment {

    private String mImeEmisije;
    private String mImePrograma;
    private Program mProgram;
    private Emisija mEmisija;
    private Callbacks mCallbacks;
    TextView mPrikaz;
    Button mOcijeni;
    NumberPicker mOcjena;
    EditText mKomentar;
    Button mPotvrdi;
    Button mSpasi;



    private static final String ARG_PROGRAM_IME =
            "com.example.korisnik.informacije.ime_programa";
    private static final String ARG_EMISIJA_IME =
            "com.example.korisnik.informacije.ime_emisije";


   /* public static Intent newIntent(Context packageContext, String imeEmisije, String imePrograma) {
        Intent intent = new Intent(packageContext, EmisijaInfoPager.class);
        intent.putExtra(EXTRA_EMISIJA_IME, imeEmisije);
        intent.putExtra(EXTRA_PROGRAM_IME, imePrograma);
        return intent;
    }*/

    public static EmisijaInfoFragment newInstance(String imeEmisije, String imePrograma) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_EMISIJA_IME, imeEmisije);
        args.putSerializable(ARG_PROGRAM_IME, imePrograma);
        EmisijaInfoFragment fragment = new EmisijaInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public interface Callbacks {
    }

    /*public static EmisijaInfoFragment newInstance(String imeEmisije, String imePrograma){
        EmisijaInfoFragment fragment = new EmisijaInfoFragment();
        fragment.mImeEmisije = imeEmisije;
        fragment.mImePrograma = imePrograma;

        return fragment;
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mEmisija = new Emisija();
        mProgram = new Program();
        mImeEmisije = (String) getArguments().getSerializable(ARG_EMISIJA_IME);
        mImePrograma = (String) getArguments().getSerializable(ARG_PROGRAM_IME);

        List<Emisija> emisije;
        emisije = mProgram.PreuzimanjeInfo(mImePrograma);

        for(Emisija em : emisije){
            if(em.getNaziv().equals(mImeEmisije)){
                mEmisija = em;
                break;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_emisija_info, container, false);


        mPrikaz = (TextView) v.findViewById(R.id.prikaz);
        mPrikaz.setText(mImeEmisije + "\n\n" + mEmisija.getZanr() + "\n\n" + mEmisija.getOpis() + "\n\n" + mEmisija.getGlumci());

        mOcjena = (NumberPicker) v.findViewById(R.id.Ocjena);
        mOcjena.setMaxValue(5);
        mOcjena.setMinValue(1);

        mKomentar = (EditText) v.findViewById(R.id.Komentar);

        mPotvrdi = (Button) v.findViewById(R.id.Potvrdi);

        mOcijeni = (Button) v.findViewById(R.id.ocijeni_button);

        mOcijeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOcjena.setVisibility(view.VISIBLE);
                mKomentar.setVisibility(view.VISIBLE);
                mPotvrdi.setVisibility(view.VISIBLE);

                mPotvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    mOcijeni.setVisibility(view.INVISIBLE);
                    mOcjena.setVisibility(view.INVISIBLE);
                    mKomentar.setVisibility(view.INVISIBLE);
                    mPotvrdi.setVisibility(view.INVISIBLE);

                    Recenzija rec = new Recenzija();
                    rec.setOcjena(mOcjena.getValue());
                    rec.setKomentar(mKomentar.getText().toString());
                    rec.setEmisija(mEmisija);
                    Calendar calendar = Calendar.getInstance();
                    rec.setDatum(calendar.getTime());

                    BazaRecenzija.get(getActivity()).dodajRecenziju(rec);
                  }
              });
            }
        });

        mSpasi = v.findViewById(R.id.Spasi);
        mSpasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");
                calIntent.putExtra(CalendarContract.Events.TITLE, mImeEmisije);
                calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, mImePrograma);
                calIntent.putExtra(CalendarContract.Events.DESCRIPTION,mEmisija.getVrijeme() + "\n" + mEmisija.getOpis());

                Date danasnjiDatum = new Date();
                GregorianCalendar calDate = new GregorianCalendar(danasnjiDatum.getYear(), danasnjiDatum.getMonth(), danasnjiDatum.getDay());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);

                startActivity(calIntent);
            }
        });

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
}

