package com.bignerdranch.android.whattowatch;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by KORISNIK on 5/30/2018.
 */

public class RecenzijeFragment extends Fragment {
    private RecyclerView mRecenzijeRecyclerView;
    private RecenzijeAdapter mAdapter;
    private Callbacks mCallbacks;


    public interface Callbacks {
        void onRecenzijaSelected(Recenzija recenzija);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_recenzija_fragment, container, false);
        mRecenzijeRecyclerView = (RecyclerView) view
                .findViewById(R.id.recenzije_recycler_view);
        mRecenzijeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_recenzija_lista, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nova_recenzija:

                Intent intent = new Intent(getActivity() , DodajRecenzijuActivity.class);
                startActivity(intent);
                updateUI();

                /*Recenzija recenzija = new Recenzija();
                BazaRecenzija.get(getActivity()).dodajRecenziju(recenzija);
                Intent intent = RecenzijaPagerActivity
                        .newIntent(getActivity(), recenzija.getId());
                startActivity(intent);
                Recenzija recenzija = new Recenzija();
                BazaRecenzija.get(getActivity()).dodajRecenziju(recenzija);
                updateUI();
                mCallbacks.onRecenzijaSelected(recenzija);*/
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateUI() {
        BazaRecenzija bazaRecenzija = BazaRecenzija.get(getActivity());
        List<Recenzija> sveRecenzije = bazaRecenzija.getRecenzije();

        if (mAdapter == null) {
            mAdapter = new RecenzijeAdapter(sveRecenzije);
            mRecenzijeRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.postaviRecenzije(sveRecenzije);
            mAdapter.notifyDataSetChanged();
        }

    }

    private class RecenzijeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNaslovTextView;
        private Recenzija mRecenzija;

        public RecenzijeHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.lista_item_program, parent, false));
            itemView.setOnClickListener(this);
            mNaslovTextView = (TextView) itemView.findViewById(R.id.program_naslov);

        }

        public void bind(Recenzija recenzija) {
            mRecenzija = recenzija;
            mNaslovTextView.setText(mRecenzija.getEmisija().getNaziv() + ", " + mRecenzija.getOcjena());
        }
        @Override
        public void onClick(View view) {
            /*Intent intent = RecenzijaPagerActivity.newIntent(getActivity(), mRecenzija.getId());

            startActivity(intent);*/
            mCallbacks.onRecenzijaSelected(mRecenzija);
        }

    }

    private class RecenzijeAdapter extends RecyclerView.Adapter<RecenzijeFragment.RecenzijeHolder> {
        private List<Recenzija> mRecenzije;
        public RecenzijeAdapter(List<Recenzija> recenzije) {
            mRecenzije = recenzije;
        }

        @Override
        public RecenzijeFragment.RecenzijeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new RecenzijeFragment.RecenzijeHolder(layoutInflater, parent);
        }
        @Override
        public void onBindViewHolder(RecenzijeFragment.RecenzijeHolder holder, int position) {
            Recenzija recenzija = mRecenzije.get(position);
            holder.bind(recenzija);
        }
        @Override
        public int getItemCount() {
            return mRecenzije.size();
        }
        public void postaviRecenzije(List<Recenzija> recenzije) {
            mRecenzije = recenzije;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

}