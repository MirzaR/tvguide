package com.bignerdranch.android.whattowatch;

/**
 * Created by User on 05.06.2018..
 */

public class Emisija {
    String naziv;
    String opis;
    String vrijeme;
    String program;
    String zanr;
    String glumci;

    public Emisija() {
        this.naziv = "";
        this.opis = "";
        this.vrijeme = "";
        this.program = "";
        this.zanr = "";
        this.glumci = "";
    }

    public Emisija(String naziv,String opis,String vrijeme,String program, String zanr, String glumci){
        this.naziv= naziv;
        this.opis=opis;
        this.vrijeme=vrijeme;
        this.program=program;
        this.zanr = zanr;
        this.glumci = glumci;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getVrijeme() {
        return vrijeme;
    }

    public void setVrijeme(String vrijeme) {
        this.vrijeme = vrijeme;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getZanr() {
        return zanr;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }

    public String getGlumci() {
        return glumci;
    }

    public void setGlumci(String glumci) {
        this.glumci = glumci;
    }
}
