package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

/**
 * Created by KORISNIK on 6/5/2018.
 */

public class RecenzijaInfoFragment extends Fragment {

    private static final String ARG_RECENZIJA_ID = "recenzija_id";

    TextView mRecenzijaInfo;
    UUID recenzijaId;
    private Recenzija mRecenzija;
    private Callbacks mCallbacks;
    private EditText mNoviKomentar;
    private NumberPicker mNovaOcjena;
    private Button mPotvrdi;

    public interface Callbacks {
        void onRecenzijaUpdated(Recenzija recenzija);
    }

    public static RecenzijaInfoFragment newInstance(UUID recenzijaId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_RECENZIJA_ID, recenzijaId);
        RecenzijaInfoFragment fragment = new RecenzijaInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_recenzija_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.izmjena_recenzije:

                mNoviKomentar.setVisibility(View.VISIBLE);
                mNovaOcjena.setVisibility(View.VISIBLE);
                mPotvrdi.setVisibility(View.VISIBLE);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recenzijaId = (UUID) getArguments().getSerializable(ARG_RECENZIJA_ID);
        mRecenzija = BazaRecenzija.get(getActivity()).getRecenzija(recenzijaId);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause(){
        super.onPause();
        BazaRecenzija.get(getActivity())
                .azuriranjeRecenzije(mRecenzija);
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_recenzija_info, container, false);

        mRecenzijaInfo = (TextView) v.findViewById(R.id.recenzija_info);
        mNoviKomentar = (EditText) v.findViewById(R.id.NoviKomentar);
        mNoviKomentar.setText(mRecenzija.getKomentar());


        mNovaOcjena = (NumberPicker) v.findViewById(R.id.NovaOcjena);
        mNovaOcjena.setMinValue(1);
        mNovaOcjena.setMaxValue(5);
        mNovaOcjena.setValue(mRecenzija.getOcjena());

        mPotvrdi = (Button) v.findViewById(R.id.Potvrdi);
        mPotvrdi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                View v = getLayoutInflater().inflate(R.layout.dialog_update_recenzija, null);
                final EditText mConfirmEdit = (EditText) v.findViewById(R.id.confirm_tekst);
                Button mConfirmBtn = (Button) v.findViewById(R.id.potvrdi_btn);
                Button mCancelBtn = (Button) v.findViewById(R.id.cancel_btn);

                mBuilder.setView(v);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();
                mConfirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mConfirmEdit.getText().toString().equals("confirm")){

                            mRecenzija.setKomentar(mNoviKomentar.getText().toString());

                            mRecenzija.setOcjena(mNovaOcjena.getValue());

                            mRecenzijaInfo.setText(mRecenzija.getEmisija().getNaziv() + "\n\n" + mRecenzija.getDatum() + ", " + mRecenzija.getEmisija().getProgram() +
                                    "\n\n\"" + mRecenzija.getKomentar() + "\"\n\nOcjena:" + mRecenzija.getOcjena());
                            mNoviKomentar.setVisibility(View.INVISIBLE);
                            mNovaOcjena.setVisibility(View.INVISIBLE);

                            BazaRecenzija.get(getActivity())
                                    .azuriranjeRecenzije(mRecenzija);
                            updateRecenzija();
                            dialog.cancel();

                        }
                        else{
                            Toast.makeText(getContext(), "Netacan unos!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                mCancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });



            }
        });

        BazaRecenzija bazaRecenzija = BazaRecenzija.get(getActivity());

        Recenzija recenzija = bazaRecenzija.getRecenzija(recenzijaId);

        mRecenzijaInfo.setText(recenzija.getEmisija().getNaziv() + "\n\n" + recenzija.getDatum() + ", " + recenzija.getEmisija().getProgram() +
                "\n\n\"" + recenzija.getKomentar() + "\"\n\nOcjena:" + recenzija.getOcjena());

        return v;

    }

    private void updateRecenzija() {

        BazaRecenzija.get(getActivity()).azuriranjeRecenzije(mRecenzija);
        mCallbacks.onRecenzijaUpdated(mRecenzija);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
}
