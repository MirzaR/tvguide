package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by KORISNIK on 5/15/2018.
 */

public class ProgramiFragment extends Fragment {

    private RecyclerView mProgramiRecyclerView;
    private ProgramiAdapter mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_programa_fragment, container, false);
        mProgramiRecyclerView = (RecyclerView) view
                .findViewById(R.id.programi_recycler_view);
        mProgramiRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();

        return view;
    }

    private void updateUI() {
        SpisakPrograma spisakPrograma = SpisakPrograma.get(getActivity());
        List<NazivPrograma> programi = spisakPrograma.getSviProgrami();
        mAdapter = new ProgramiAdapter(programi);
        mProgramiRecyclerView.setAdapter(mAdapter);
    }

    private class ProgramiHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNaslovTextView;
        private NazivPrograma mNazivPrograma;

        public ProgramiHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.lista_item_program, parent, false));
            itemView.setOnClickListener(this);
            mNaslovTextView = (TextView) itemView.findViewById(R.id.program_naslov);

        }

        public void bind(NazivPrograma nazivPrograma) {
            mNazivPrograma = nazivPrograma;
            mNaslovTextView.setText(mNazivPrograma.getIme());
        }
        @Override
        public void onClick(View view) {
            Intent intent = EmisijaActivity.newIntent(getActivity(), mNazivPrograma.getIme());
            startActivity(intent);
        }

    }

    private class ProgramiAdapter extends RecyclerView.Adapter<ProgramiHolder> {
        private List<NazivPrograma> mNaziviPrograma;
        public ProgramiAdapter(List<NazivPrograma> programi) {
            mNaziviPrograma = programi;
        }

        @Override
        public ProgramiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new ProgramiHolder(layoutInflater, parent);
        }
@Override
        public void onBindViewHolder(ProgramiHolder holder, int position) {
            NazivPrograma program = mNaziviPrograma.get(position);
            holder.bind(program);
        }
        @Override
        public int getItemCount() {
            return mNaziviPrograma.size();
        }
    }

}
