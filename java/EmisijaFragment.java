package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 05.06.2018..
 */

public class EmisijaFragment extends Fragment {

    private RecyclerView mEmisijeRecyclerView;
    private EmisijaFragment.EmisijeAdapter mAdapter;
    private Program mProgram;
    // private EmisijaActivity mEmisijaActivity;
    public List<Emisija> mSveEmisije;
    public String mImePrograma;

    private Callbacks mCallbacks;

    public interface Callbacks {
        void onEmisijaSelected(String nazivEmisije, String nazivPrograma);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        mProgram = new Program();
        // mEmisijaActivity = new EmisijaActivity();
        View view = inflater.inflate(R.layout.lista_emisija_fragment, container, false);
        mEmisijeRecyclerView = (RecyclerView) view
                .findViewById(R.id.emisije_recycler_view);
        mEmisijeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();

        return view;
    }

    public void updateUI() {
        mImePrograma = (String) getActivity().getIntent()
                .getSerializableExtra(EmisijaActivity.EXTRA_PROGRAM_IME);
        Log.v("Pomoc1",mImePrograma);
        mSveEmisije = mProgram.PreuzimanjeInfo(mImePrograma);

        mAdapter = new EmisijaFragment.EmisijeAdapter(mSveEmisije);
        mEmisijeRecyclerView.setAdapter(mAdapter);
    }

    private class EmisijeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNaslovTextView;
        private String mNazivEmisije;
        private String mVrijemeEmisije;

        public EmisijeHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.lista_item_program, parent, false));
            itemView.setOnClickListener(this);
            mNaslovTextView = (TextView) itemView.findViewById(R.id.program_naslov);

        }

        public void bind(Emisija emisija) {
            mNazivEmisije = emisija.getNaziv();
            mVrijemeEmisije = emisija.getVrijeme();
            mNaslovTextView.setText(mVrijemeEmisije +" -> " + mNazivEmisije);
        }
        @Override
        public void onClick(View view) {
            mCallbacks.onEmisijaSelected(mNazivEmisije, mImePrograma);
        }

    }

    private class EmisijeAdapter extends RecyclerView.Adapter<EmisijaFragment.EmisijeHolder> {
        private List<Emisija> mSveEmisije;
        public EmisijeAdapter(List<Emisija> emisije) {
            mSveEmisije = emisije;
        }

        @Override
        public EmisijaFragment.EmisijeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new EmisijaFragment.EmisijeHolder(layoutInflater, parent);
        }
        @Override
        public void onBindViewHolder(EmisijaFragment.EmisijeHolder holder, int position) {
            Emisija emisija = mSveEmisije.get(position);
            holder.bind(emisija);
        }
        @Override
        public int getItemCount() {
            return mSveEmisije.size();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }
}

