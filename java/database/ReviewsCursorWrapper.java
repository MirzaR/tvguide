package com.bignerdranch.android.whattowatch.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.whattowatch.Emisija;
import com.bignerdranch.android.whattowatch.Recenzija;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by User on 05.06.2018..
 */

public class ReviewsCursorWrapper extends CursorWrapper {
    public ReviewsCursorWrapper(Cursor cursor) {
        super(cursor);
    }
    public Recenzija getRecenzija() {
        String uuidString = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.UUID));
        String naziv = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.ImeEmisije));
        String vrijeme = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Vrijeme));
        String zanr = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Zanr));
        String glumci = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Glumci));
        String opis = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Opis));
        String komenar = getString(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Komentar));
        int ocjena = getInt(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Ocjena));

        long datum = getLong(getColumnIndex(RecenzijaDbSchema.RecenzijaTable.Cols.Datum));

        Recenzija recenzija = new Recenzija(UUID.fromString(uuidString));
        Emisija emisija = new Emisija(naziv,opis,vrijeme,"TV1000",zanr,glumci);

        recenzija.setEmisija(emisija);
        Calendar calendar = Calendar.getInstance();
        recenzija.setDatum(calendar.getTime());
        recenzija.setKomentar(komenar);
        recenzija.setOcjena(ocjena);
        return recenzija;
    }
}

