package com.bignerdranch.android.whattowatch;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bignerdranch.android.whattowatch.database.BazaRecenzijaPomocnik;
import com.bignerdranch.android.whattowatch.database.RecenzijaDbSchema;
import com.bignerdranch.android.whattowatch.database.ReviewsCursorWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by User on 05.06.2018..
 */

public class BazaRecenzija {
    private static BazaRecenzija sSpisakRecenzija;
    private List<Recenzija> mSveRecenzije;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    private BazaRecenzija(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new BazaRecenzijaPomocnik(mContext)
                .getWritableDatabase();

    }
    public static BazaRecenzija get(Context context) {
        if (sSpisakRecenzija == null) {
            sSpisakRecenzija = new BazaRecenzija(context);
        }
        return sSpisakRecenzija;

    }


    public void dodajRecenziju(Recenzija rec) {
        try {
            ContentValues values = getContentValues(rec);
            mDatabase.insert(RecenzijaDbSchema.RecenzijaTable.NAME, null, values);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public List<Recenzija> getRecenzije() {
        List<Recenzija> recenzije = new ArrayList<>();
        ReviewsCursorWrapper cursor = queryReviews(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                recenzije.add(cursor.getRecenzija());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return recenzije;
    }

    public Recenzija getRecenzija(UUID id) {
        ReviewsCursorWrapper cursor = queryReviews(
                RecenzijaDbSchema.RecenzijaTable.Cols.UUID + " = ?",
                new String[] { id.toString() }
        );
        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getRecenzija();
        } finally {
            cursor.close();
        }


    }

    public void azuriranjeRecenzije(Recenzija recenzija) {
        String uuidString = recenzija.getId().toString();
        ContentValues values = getContentValues(recenzija);
        mDatabase.update(RecenzijaDbSchema.RecenzijaTable.NAME, values,
                RecenzijaDbSchema.RecenzijaTable.Cols.UUID + " = ?",
                new String[] { uuidString });
    }

    private static ContentValues getContentValues(Recenzija recenzija) {
        ContentValues values = new ContentValues();
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Vrijeme, recenzija.getEmisija().getVrijeme());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Komentar, recenzija.getKomentar());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.UUID, recenzija.getId().toString());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.ImeEmisije, recenzija.getEmisija().getNaziv());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Opis, recenzija.getEmisija().getOpis());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Datum,  recenzija.getDatum().toString());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Ocjena, Integer.toString(recenzija.getOcjena()));
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Glumci, recenzija.getEmisija().getGlumci());
        values.put(RecenzijaDbSchema.RecenzijaTable.Cols.Zanr, recenzija.getEmisija().getZanr());

        return values;
    }
    private ReviewsCursorWrapper queryReviews(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                RecenzijaDbSchema.RecenzijaTable.NAME,
                null, // columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new ReviewsCursorWrapper(cursor);
    }
}
