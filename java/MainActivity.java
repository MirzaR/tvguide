package com.bignerdranch.android.whattowatch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button mProgram;
    Button mRecenzije;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgram = (Button) findViewById(R.id.Program);
        mProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,
                        ProgramActivity.class);
                startActivity(myIntent);

            }
        });
        mRecenzije = (Button) findViewById(R.id.Recenzije);
        mRecenzije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,
                        RecenzijeActivity.class);
                startActivity(myIntent);

            }
        });




    }

}


