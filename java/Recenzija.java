package com.bignerdranch.android.whattowatch;

import java.util.Date;
import java.util.UUID;

/**
 * Created by KORISNIK on 5/24/2018.
 */

public class Recenzija {

    private UUID mId;
    Emisija mEmisija;
    String mKomentar;
    int mOcjena;
    Date mDatum;

    public Recenzija(Emisija em,String ko,int oc){
        this(UUID.randomUUID());
        mEmisija=em;
        mKomentar=ko;
        mOcjena=oc;

    }
    public Recenzija(){
        this(UUID.randomUUID());
    }
    public Recenzija(UUID id) {
        mId = id;
        mDatum = new Date();
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public Emisija getEmisija() {
        return mEmisija;
    }

    public void setEmisija(Emisija emisija) {
        mEmisija = emisija;
    }

    public String getKomentar() {
        return mKomentar;
    }

    public void setKomentar(String komentar) {
        mKomentar = komentar;
    }

    public int getOcjena() {
        return mOcjena;
    }

    public void setOcjena(int ocjena) {
        mOcjena = ocjena;
    }

    public Date getDatum() {
        return mDatum;
    }

    public void setDatum(Date datum) {
        mDatum = datum;
    }
}

