package com.bignerdranch.android.whattowatch;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DodajRecenzijuActivity extends AppCompatActivity {

    private Spinner mListaPrograma;
    List<String> mSviProgrami;
    ArrayAdapter<String> adapter;
    String mOdabraniProgram;
    Program mProgram;

    private Spinner mListaEmisija;
    List<Emisija> mSveEmisije;
    List<String> mImenaEmisija;
    ArrayAdapter<String> adapter1;
    Emisija mOdabranaEmisija;
    String mOdabranaEmisijaIme;

    private EditText mKomentar;
    private NumberPicker mOcjena;
    private Button mSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_recenziju);

        mProgram = new Program();
        mOdabraniProgram = "TV1000";
        mSveEmisije = new ArrayList<>();

        mSveEmisije = mProgram.PreuzimanjeInfo(mOdabraniProgram);
        mImenaEmisija = new ArrayList<>();

        for(int i=0; i<mSveEmisije.size()/2; i++) mImenaEmisija.add(mSveEmisije.get(i).getNaziv());

        mListaPrograma = findViewById(R.id.lista_programa);

        mSviProgrami = new ArrayList<>();
        mSviProgrami.add("TV1000");
        mSviProgrami.add("FOX");
        mSviProgrami.add("FOXlife");
        mSviProgrami.add("CineStar");
        mSviProgrami.add("TV1");
        mSviProgrami.add("RTL");
        mSviProgrami.add("FoxMovie");
        mSviProgrami.add("NovaTV");
        mSviProgrami.add("Hayat");
        mSviProgrami.add("FTV");
        mSviProgrami.add("FaceTV");
        mSviProgrami.add("OBN");
        mSviProgrami.add("HRT1");
        mSviProgrami.add("Pink");
        mSviProgrami.add("AXN");

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mSviProgrami);

        mListaPrograma.setAdapter(adapter);


        mListaPrograma.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                mOdabraniProgram = parent.getItemAtPosition(position).toString();
                Log.v("odabrani program", mOdabraniProgram);
                mListaEmisija = findViewById(R.id.lista_emisija);


                mSveEmisije = mProgram.PreuzimanjeInfo(mOdabraniProgram);

                for(int i = 0; i < mSveEmisije.size(); i++) mImenaEmisija.add(mSveEmisije.get(i).getNaziv());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        mListaEmisija = findViewById(R.id.lista_emisija);


        adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mImenaEmisija);
        mListaEmisija.setVisibility(View.VISIBLE);

        mListaEmisija.setAdapter(adapter1);
        mListaEmisija.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                mOdabranaEmisijaIme = parent.getItemAtPosition(position).toString();
                mOdabranaEmisija = mSveEmisije.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        mKomentar = findViewById(R.id.komentar);

        mOcjena = findViewById(R.id.ocjena);
        mOcjena.setMaxValue(5);
        mOcjena.setMinValue(1);

        mSubmit = findViewById(R.id.submit);
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Recenzija recenzija = new Recenzija(mOdabranaEmisija, mKomentar.getText().toString(), mOcjena.getValue());
                Calendar calendar = Calendar.getInstance();
                recenzija.setDatum(calendar.getTime());
                BazaRecenzija.get(DodajRecenzijuActivity.this).dodajRecenziju(recenzija);
                Intent intent = new RecenzijeActivity().newIntent(getApplicationContext());
                startActivity(intent);
            }
        });

    }
}

