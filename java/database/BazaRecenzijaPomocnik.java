package com.bignerdranch.android.whattowatch.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 05.06.2018..
 */

public class BazaRecenzijaPomocnik extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "recenzijeBase.db";
    public BazaRecenzijaPomocnik(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + RecenzijaDbSchema.RecenzijaTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                RecenzijaDbSchema.RecenzijaTable.Cols.UUID + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.ImeEmisije + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Datum + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Vrijeme + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Zanr + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Glumci + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Opis + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Komentar + ", " +
                RecenzijaDbSchema.RecenzijaTable.Cols.Ocjena +
                ")"
        );
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
