package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by User on 05.06.2018..
 */

public class EmisijaActivity extends SingleFragmentActivity
        implements EmisijaFragment.Callbacks, EmisijaInfoFragment.Callbacks{

    public static final String EXTRA_PROGRAM_IME =
            "com.example.korisnik.informacije.ime_programa";
    public static Intent newIntent(Context packageContext, String imePrograma) {
        Intent intent = new Intent(packageContext, EmisijaActivity.class);
        intent.putExtra(EXTRA_PROGRAM_IME, imePrograma);
        return intent;
    }

    @Override
    protected Fragment createFragment() {

        return new EmisijaFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail1;
    }

    @Override
    public void onEmisijaSelected(String nazivEmisije, String nazivPrograma) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = EmisijaInfoPager.newIntent(this, nazivEmisije, nazivPrograma);
            startActivity(intent);
        } else {
            Fragment newDetail = EmisijaInfoFragment.newInstance(nazivEmisije, nazivPrograma);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, newDetail)
                    .commit();
        }
    }
}
