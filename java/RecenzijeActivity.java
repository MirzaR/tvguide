package com.bignerdranch.android.whattowatch;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

public class RecenzijeActivity extends SingleFragmentActivity
implements RecenzijeFragment.Callbacks, RecenzijaInfoFragment.Callbacks{

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, RecenzijeActivity.class);
        return intent;
    }

    @Override
    protected Fragment createFragment() {

        return new RecenzijeFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    public void onRecenzijaSelected(Recenzija recenzija){
        if (findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = RecenzijaPagerActivity.newIntent(this, recenzija.getId());
            startActivity(intent);
        } else {
            Fragment newDetail = RecenzijaInfoFragment.newInstance(recenzija.getId());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, newDetail)
                    .commit();
        }
    }

    public void onRecenzijaUpdated(Recenzija recenzija){
        RecenzijeFragment recenzijeFragment = (RecenzijeFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        recenzijeFragment.updateUI();
    }

}

